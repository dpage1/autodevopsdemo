#!/usr/bin/env sh

## Docker params
IMAGE=klokantech/tileserver-gl
VERSION=v1.7.0
CONTAINER=tileserv
MOUNTS="-v ./data:/data:ro"
OTHERARGS="--restart always --name $CONTAINER"

echo -n "Stopping container: $CONTAINER"
docker stop $CONTAINER
echo -n "Removing container: "
docker rm $CONTAINER

echo "Creating container: "
docker create $MOUNTS $OTHERARGS -p 8080:80 $IMAGE:$VERSION $COMMAND

echo -n "Starting container: "
docker start $CONTAINER
