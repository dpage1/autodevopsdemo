<h1><a id="Watchman_Auto_Deployment_via_GitLab_Runner_to_K8_0"></a>Watchman Auto Deployment via GitLab Runner to K8</h1>
<p>Used a K8 cluster out on AWS to test out the deployment</p>
<p>Be sure to set the variables in GitLab prior to execution these need to be defined in the environment vars section of the GitLab project</p>
<ul>
<li>DOCKER_REGISTRY</li>
<li>DOCKER_UNAME</li>
<li>DOCKER_PWD</li>
<li>DOCKER_EMAIL</li>
<li>GITLAB_URL (this is URL where individual W4D components deployment YAMLs live)</li>
<li>NAME (this is the name of the K8 Cluster that you are deploying to)</li>
<li>KOPS_STATE_STORE (this is the location that which the Cluster state is kept)</li>
</ul>
<p>If you are going to need to sign into the Docker Registry and setup keys for your user then uncomment the signin-docker-repo: section of the YAML</p>
<p>Be sure to change the the tags throughout the YAML to point to the GitLab Runner that you intend to use for the deployment (currently it is set to use the GitLab Runner that is installed on the jumpbox on AWS)</p>
<p>Review all the W4D deployment YAML files to ensure they are pulling the docker containers from the correct repository (Unclass DI2E vs CIE)</p>
<p>I was not able to fully test out via GUI need the URLs (chad can assist)</p>
<ul>
<li>W4D app should be HTTP(S)://&lt;LoadBalancerIP&gt;:7000/webclient/waApp/#!/login</li>
<li>Don’t know what the URL would be for the map server need to reach out to W4D team</li>
</ul>
<p>Was able to complete the deployment as the containers and services can be seen as deployed, up and runner for all W4D component (no errors)</p>
<p>Disclaimer: More work can and should be done to further make this deployment further agnostic and dynamic but for the early stages of deployment this does work but does still need to be verified.</p>
<p>Additional Information</p>
<p>Setting up and configuring RHEL 73 GitLab Runner (Unclass side)</p>
<ul>
<li>To replicate this in the CIE packages need to be collected and brought in.</li>
</ul>
<p>Add GitLab Repo<br>
curl -L <a href="https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh">https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh</a> | sudo bash</p>
<p>List the available runners to install by version<br>
sudo yum list gitlab-runner --showduplicates | sort -r</p>
<p>Install a specific version of the GitLab Runner<br>
sudo yum install gitlab-runner-&lt;version&gt;<br>
EX: sudo yum install gitlab-runner-11.6.1-1</p>
<p>Setup comms between runner and cluster on AWS:</p>
<ul>
<li>setup ssh key pair as ec2-user NOTE: ONLY DO THIS IF YOU NEED TO ESTABLISH NEW COMMS ON A NEW EC2<br>
ssh-keygen -t rsa<br>
kops create secret --name &lt;cluster_name_in_EKS&gt;.k8s.local sshpublickey admin -i ~/.ssh/id_rsa.pub</li>
</ul>
<p>Register your newly created runner w/ the GitLab server<br>
<a href="https://docs.gitlab.com/runner/register/">https://docs.gitlab.com/runner/register/</a><br>
sudo gitlab-runner register</p>
<ul>
<li>Enter the gitlab coordinator URL == EX <a href="https://gitlab.com">https://gitlab.com</a></li>
<li>Enter the gitlab token for this runner == Get this from the GitLab Server</li>
<li>Describe the Runner</li>
<li>Enter Tags == EX AWS RHEL73 Runner1…</li>
<li>Define the executor = EX docker kubernetes etc…
<ul>
<li>If you choose Docker as the executor you will have to tell it which image to use == alpine:latest</li>
</ul>
</li>
</ul>
<p>When using AWS ensure that you setup and pass in all the K8 information for .kube under the /home/gitlab-runner dir<br>
Generally everything is setup with the ec2-user uname therefore you have copy everythign over into the gitlab-runner’s user dir<br>
sudo chmod 755 /home/gitlab-runner<br>
sudo chown gitlabrunner:gitlabrunner /home/gitlab-runner/*</p>
<p>Be sure to setup your Docker Login on the runner so it can authenticate and pull images<br>
From the .gitlab-ci.yml file:</p>
<ul>
<li>signin-docker-repo:</li>
<li>stage: signin</li>
<li>tags:</li>
<li>
<pre><code>(#) k8
</code></pre>
</li>
<li>
<pre><code>- jumpbox
</code></pre>
</li>
<li>script:</li>
<li>
<pre><code>kubectl create secret docker-registry regcred --docker-server='$DOCKER_REGISTRY'   docker-username='$DOCKER_UNAME' --docker-password='$DOCKER_PWD' --docker-email='$DOCKER_EMAIL'
</code></pre>
</li>
</ul>
<p>If you are using kubernetes you will need to setup the cluster so that the project can interact with it<br>
<a href="https://gitlab.com/help/user/project/clusters/index">https://gitlab.com/help/user/project/clusters/index</a></p>
<ul>
<li>Under Operations --&gt; Kubernetes</li>
<li>Set the Kubernetes cluster name</li>
<li>Set the Environment scope ==   - <a href="https://gitlab.com/help/user/project/clusters/index#setting-the-environment-scope-premium">https://gitlab.com/help/user/project/clusters/index#setting-the-environment-scope-premium</a></li>
<li>Set the API URL  –  use the command below<br>
kubectl cluster-info | grep ‘Kubernetes master’ | awk ‘/http/ {print $NF}’</li>
<li>Set the CA Certificate  – use the command below<br>
kubectl get secrets  – this will list the secrets<br>
kubectl get secret &lt;secret name&gt; -o jsonpath=&quot;{[‘data’][‘ca.crt’]}&quot; | base64 --decode</li>
<li>Set the git-admin token using the gitlab-admin-service-account.yaml file<br>
kubectl apply -f gitlab-admin-service-account.yaml<br>
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk ‘{print $1}’)</li>
</ul>
<p>Cleaning up the deployment<br>
kubectl get deployment<br>
kubectl delete deployment &lt;deployment name&gt;<br>
kubectl get svc<br>
kubectl delete svc &lt;service name&gt;</p>
<p>Teardown cluster in AWS<br>
kops delete cluster --name ${NAME} --yes</p>